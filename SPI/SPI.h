/*
 * SPI.h
 *
 * Created: 9/5/2017 8:34:54 PM
 *  Author: Cennan
 */ 


#ifndef SPI_H_
#define SPI_H_

void SPI_MasterInit(void);
void SPI_MasterTransmit(char Data);

void SPI_SlaveInit(void);
char SPI_SlaveReceive(void);

#endif /* SPI_H_ */