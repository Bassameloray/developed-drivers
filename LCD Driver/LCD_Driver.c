#include "LCD_Driver.h"																										/* LCD Functions*/
/************************************ General Functions That Work For Both Modes ***********************************************/
void SendFallingEdge()																										/* Gives The Controller A Falling Edge To Get The Command Executed*/
{																															/* */
	DIO_VSetPort_IO('B',3,'h');																								/* Sets DDRB PORT3 To Output*/
	_delay_ms(2);																											/* Gives The Controller A Chance To Take Action Before Doing The Next Command*/
	DIO_VSetPort_IO('B',3,'l');																								/* Sets DDRB PORT3 To Input To Create Falling Edge*/
	_delay_ms(2);																											/* Gives The Controller A Chance To Take Action Before Doing The Next Command*/
}																															/* */
/*********************************************** 8 Bits LCD Functions **********************************************************/
																															/* If The 8 Bits Mode Is Enabled This Block WIll Be Compiled*/
void LCD_vInit_8BitMode()																									/* */
{																															/* Makes The Initialization For The 8 Bits LCD Mode*/
	DIO_VSetAllPinDir('A','h');																								/* Sets All DDRA To ON*/
	DIO_VSetPinDir('B',1,'h');																								/* Sets DDRB Port1 To On*/
	DIO_VSetPinDir('B',2,'h');																								/* Sets DDRB Port2 To On*/
	DIO_VSetPinDir('B',3,'h');																								/* Sets DDRB Port3 To On*/
	DIO_VSetPort_IO('B',2,'l');																								/* Sets DDRB Port2 To Input*/
	LCD_vSendCmd_8BitMode(MODE_8_BITS);																						/* Sends A Command For THE LCD To Work As 8 Bits LCD*/
	LCD_vSendCmd_8BitMode(CLEAR_LCD);																						/* Sends A Command To Clear The LCD*/
	LCD_vSendCmd_8BitMode(CURSOR_ON);																						/* Sends A Command To Turn The Cursor On*/
}																															/* */
																															/* */
void LCD_vSendCmd_8BitMode(char cmd)																						/* Send Command To The Controller For The LCD 8 Bits Mode*/
{																															/* */
	PORTA = cmd;																											/* Sends The Command To PORTA*/
	DIO_VSetPort_IO('B',1,'l');																								/* Sets DDRB Port1 To Input*/
	SendFallingEdge();																										/* Sends A Falling Edge To Get The Command Executed*/
}																															/* */
																															/* */
void LCD_vSendData_8BitMode(char data)																						/* Sends Data To The Controller For The LCD 8 Bits Mode*/
{																															/* */
	PORTA = data;																											/* Sends The Data To PORTA*/
	DIO_VSetPort_IO('B',1,'h');																								/* Sets DDRB Port1 To Output*/
	SendFallingEdge();																										/* */
}																															/* Sends Data To The Controller For The LCD 8 Bits Mode*/
																															/* */
void LCD_vPrintString_8BitMode(char *x)																						/* Prints A String To The LCD 8 Bits Mode*/
{																															/* */
	char Char_Counter = 0;																									/* */
	while(*x)																												/* A While Loop The Will Go On Until It Finds A Terminator Which In This Case The End Of The String*/
	{																														/* */
		if(Char_Counter<=16)																								/* If The Char_Counter Is Less Than 16 It Will Write On The First Row */
		LCD_vSendData_8BitMode(*x++);																						/* Will Print The First Character The Increments The Pointer A Step To Print The Next Character*/
		else if(Char_Counter>16)																							/* If Char_Counter Exceeded The 16 Column */
		{																													/* */
			LCD_vSendCmd_8BitMode(SECOND_ROW_CURSOR_HOME);																	/* It Will Set The Cursor Home To The Second Row */
			while(*x)																										/* Complete The Rest Of The String */
			{																												/* */
				if(Char_Counter<=32)																						/* Making Sure The String Didn't Exceed The 32 Character*/
				LCD_vSendData_8BitMode(*x++);																				/* Prints The Rest Of The String */
				else																										/* Misra C Practice */
				{																											/* */
					;																										/* */
				}																											/* */
				Char_Counter++;																								/* Will Add 1 To The Char_Counter Which Checks For The Number Of The Characters Printed */
			}																												/* */
		}																													/* */
		else																												/* Misra C Practice */
		{																													/* */
			;																												/* */
		}																													/* */
		Char_Counter++;																										/* */
	}																														/* */
}																															/* */
																															/* */
void LCD_Go_To_X_Y_8BitsMode(unsigned char row, unsigned char column)														/* Moves The Cursor To A Certain Place On The LCD*/
{																															/* */
	char temp;																												/* */
	if(((column<=COLUMN8)&&(column>COLUMN0))&&((row<=SECOND_ROW)&&(row>Zero_ROW)))											/* Checks If The Location Is Within The LCD*/
	{																														/* */
		if(FIRST_ROW == row)																								/* If First Row*/
		{																													/* */
			temp = FIRST_ROW_CURSOR_HOME + column -1;																		/* It Will Set The Cursor To It's Home Location For The Rows, Then Will Move It To The Desired Column Location*/
			LCD_vSendCmd_8BitMode(temp);																					/* Then It Will Send The Command To The LCD To Move The Cursor*/
		}																													/* */
		else if(SECOND_ROW == row)																							/* If Second Row*/
		{																													/* */
			temp = SECOND_ROW_CURSOR_HOME + column -1;																		/* It Will Set The Cursor Location To The Seconds Row Home Location Then To The Desired Column Location*/
			LCD_vSendCmd_8BitMode(temp);																					/* Then It Will Send The Command To The Controller*/
		}																													/* */
		else																												/* Misra C Practice*/
		{																													/* */
			;																												/* */
		}																													/* */
	}																														/* */
	else if(((column>COLUMN8)||(column<=COLUMN0))||((row>SECOND_ROW)||(row<=Zero_ROW)))										/* If The Location Is Wrong*/
	{																														/* */
		LCD_vPrintString_8BitMode("N/A Location.");																			/* It Will Print This Message*/
		_delay_ms(2000);																									/* Delay For 2 Seconds*/
		LCD_vSendCmd_8BitMode(CLEAR_LCD);																					/* Clear The LCD*/
		LCD_vSendCmd_8BitMode(FIRST_ROW_CURSOR_HOME);																		/* Then Returns The Cursore To The First Row - First Column*/
	}																														/* */
	else																													/* Misra C Practice*/
	{																														/* */
		;																													/* */
	}																														/* */
}																															/* */
																															/* */
/*******************************************************************************************************************************/
/********************************************************* 4 Bits **************************************************************/
																															/* If The 4 Bits Mode Is Enabled This Block WIll Be Compiled*/
void LCD_vInit_4BitMode(void)																								/* Makes The Initialization For The 4 Bits LCD Mode*/
{																															/* */
	DIO_VSetPinDir('A',4,'h');																								/* Sets DDRA Port4 To On*/
	DIO_VSetPinDir('A',5,'h');																								/* Sets DDRA Port5 To On*/
	DIO_VSetPinDir('A',6,'h');																								/* Sets DDRA Port6 To On*/
	DIO_VSetPinDir('A',7,'h');																								/* Sets DDRA Port7 To On*/
	DIO_VSetPinDir('B',1,'h');																								/* Sets DDRB Port1 To On*/
	DIO_VSetPinDir('B',2,'h');																								/* Sets DDRB Port2 To On*/
	DIO_VSetPinDir('B',3,'h');																								/* Sets DDRB Port3 To On*/
	DIO_VSetPort_IO('B',2,'l');																								/* Sets DDRB Port2 To Input*/
	LCD_vSendCmd_4BitMode(RETURN_CURSOR_TO_HOME);																			/* Returns Cursor To It's Home Location*/
	LCD_vSendCmd_4BitMode(MODE_4_BITS);																						/* Sends A Command For THE LCD To Work As 8 Bits LCD*/
	LCD_vSendCmd_4BitMode(CLEAR_LCD);																						/* Sends A Command To Clear The LCD*/
	LCD_vSendCmd_4BitMode(CURSOR_ON);																						/* Sends A Command To Turn The Cursor On*/
	_delay_ms(20);																											/* Gives The Controller A Chance To Take Initialize And Stabilize Before Starting To Do Commands*/
}																															/* */
																															/* */
void LCD_vSendCmd_4BitMode(char Cmd_Needed)																					/* Send Command To The Controller For The LCD 4 Bits Mode*/
{																															/* */
	PORTA = (Cmd_Needed&(0xf0));																							/* Sends The Last 4 Bits Of The Command To The Last 4 Bits On PORTA*/
	DIO_VSetPort_IO('B',1,'l');																								/* Sets DDRB Port1 To Input*/
	SendFallingEdge();																										/* Sends A Falling Edge To Get The Command Executed*/
	_delay_ms(5);																											/* Delays For 5 MS*/
	PORTA = (Cmd_Needed<<4);																								/* Sends The Rest Of The Command's Bits To The Last 4 Bits On PORTA*/
	DIO_VSetPort_IO('B',1,'l');																								/* Sets DDRB Port1 To Input*/
	SendFallingEdge();																										/* Sends A Falling Edge To Get The Command Executed*/
}																															/* Delays For 5 MS*/
																															/* */
void LCD_vSendData_4BitMode(char data)																						/* Sends Data To The Controller For The LCD 4 Bits Mode*/
{																															/* */
	PORTA = (data&(0xf0));																									/* Sends The Last 4 Bits Of The Data To The Last 4 Bits On PORTA*/
	DIO_VSetPort_IO('B',1,'h');																								/* Sets DDRB Port1 To Input*/
	SendFallingEdge();																										/* Sends A Falling Edge To Get The Command Executed*/
	_delay_ms(5);																											/* Delays For 5 MS*/
	PORTA = (data<<4);																										/* Sends The Rest Of The Data's Bits To The Last 4 Bits On PORTA*/
	DIO_VSetPort_IO('B',1,'h');																								/* Sets DDRB Port1 To Input*/
	SendFallingEdge();																										/* Sends A Falling Edge To Get The Command Executed*/
}																															/* */
																															/* */
void LCD_vPrintString_4BitMode(char *x)																						/* Prints A String To The LCD 8 Bits Mode*/
{																															/* */
	char Char_Counter = 0;																									/* */
	while(*x)																												/* A While Loop The Will Go On Until It Finds A Terminator Which In This Case The End Of The String*/
	{																														/* */
		if(Char_Counter<=16)																								/* If The Char_Counter Is Less Than 16 It Will Write On The First Row */
		LCD_vSendData_4BitMode(*x++);																						/* Will Print The First Character The Increments The Pointer A Step To Print The Next Character*/
		else if(Char_Counter>16)																							/* If Char_Counter Exceeded The 16 Column */
		{																													/* */
			LCD_vSendCmd_4BitMode(SECOND_ROW_CURSOR_HOME);																	/* It Will Set The Cursor Home To The Second Row */
			while(*x)																										/* Complete The Rest Of The String */
			{																												/* */
				if(Char_Counter<=32)																						/* Making Sure The String Didn't Exceed The 32 Character*/
				LCD_vSendData_4BitMode(*x++);																				/* Prints The Rest Of The String */
				else																										/* Misra C Practice */
				{																											/* */
					;																										/* */
				}																											/* */
				Char_Counter++;																								/* Will Add 1 To The Char_Counter Which Checks For The Number Of The Characters Printed */
			}																												/* */
		}																													/* */
		else																												/* Misra C Practice */
		{																													/* */
			;																												/* */
		}																													/* */
		Char_Counter++;																										/* */
	}																														/* */
}																															/* */
																															/* */
void LCD_Go_To_X_Y_4BitsMode(unsigned char row, unsigned char column)														/* Moves The Cursor To A Certain Place On The LCD*/
{																															/* */
	char temp;																												/* */
	if(((column<=COLUMN8)&&(column>COLUMN0))&&((row<=SECOND_ROW)&&(row>Zero_ROW)))											/* Checks If The Location Is Within The LCD*/
	{																														/* */
		if(FIRST_ROW == row)																								/* If First Row*/
		{																													/* */
			temp = FIRST_ROW_CURSOR_HOME + column -1;																		/* It Will Set The Cursor To It's Home Location For The Rows, Then Will Move It To The Desired Column Location*/
			LCD_vSendCmd_4BitMode(temp);																					/* Then It Will Send The Command To The LCD To Move The Cursor*/
		}																													/* */
		else if(SECOND_ROW == row)																							/* If Second Row*/
		{																													/* */
			temp = SECOND_ROW_CURSOR_HOME + column -1;																		/* It Will Set The Cursor Location To The Seconds Row Home Location Then To The Desired Column Location*/
			LCD_vSendCmd_4BitMode(temp);																					/* Then It Will Send The Command To The Controller*/
		}																													/* */
		else																												/* Misra C Practice*/
		{																													/* */
			;																												/* */
		}																													/* */
	}																														/* */
	else if(((column>COLUMN8)||(column<=COLUMN0))||((row>SECOND_ROW)||(row<=Zero_ROW)))										/* If The Location Is Wrong*/
	{																														/* */
		LCD_vPrintString_4BitMode("N/A Location.");																			/* It Will Print This Message*/
		_delay_ms(2000);																									/* Delay For 2 Seconds*/
		LCD_vSendCmd_4BitMode(CLEAR_LCD);																					/* Clear The LCD*/
		LCD_vSendCmd_4BitMode(FIRST_ROW_CURSOR_HOME);																		/* Then Returns The Cursore To The First Row - First Column*/
	}																														/* */
	else																													/* Misra C Practice*/
	{																														/* */
		;																													/* */
	}																														/* */
}																															/* */
/***************************************************** End Of Text *************************************************************/
/*******************************************************************************************************************************/