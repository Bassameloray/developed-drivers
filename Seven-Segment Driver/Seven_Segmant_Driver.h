/***********************************************************************************/
#ifndef SEVEN_SEGMANT_DRIVER_H_                                                 /* */
#define SEVEN_SEGMANT_DRIVER_H_                                                 /* */
/***********************************************************************************/
void Sev_Seg_Init(void);                                                        /* Initialize the 7-Seg*/
void Sev_Seg_One_Write(char data);                                              /* Writes to the 1st 7-seg */
void Sev_Seg_Two_Write(char data);                                              /* Writes to the 2nd 7-seg */
void Sev_Seg_Both_Write(char data1, char data2);                                /* Writes to both 7-seg*/
#endif                                                                          /* */
/***********************************************************************************/
