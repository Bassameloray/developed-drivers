/************************************************************************************/
#include <avr/io.h>																															/* */
#define F_CPU 8000000UL																													/* */
#include <avr/delay.h>																													/* */
#include "Seven_Segmant_Driver.h"																								/* */
/***********************************************************************************/
void Sev_Seg_Init(void)																													/* Function to Initialize the Seven Seg */
{																																								/* */
	DDRC = 0xFC;																																	/* Open Bits From 2 To 7 as output */
	PORTC =0;																																			/* no voltage on the pins */
}																																								/* */
																																								/* */
void Sev_Seg_One_Write(char data)																								/* Writes to the 1st 7-Seg */
{																																								/* */
	PORTC &= 0x0f;																																/* Clears the 2nd 4 bits */
	PORTC &= ~(1<<3);																															/* clears the 3rd bit to turn off the 2nd 7-seg */
	PORTC |= (1<<2);																															/* sets the 2nd bit to 1 to turn on the 1st 7-seg */
	/* So port C now = 0000 0100 */																								/* */
	PORTC |= (data<<4);																														/* sends the data to the 2nd 4 bits */
}																																								/* */
																																								/* */
void Sev_Seg_Two_Write(char data)																								/* Writes to the 2nd 7-Seg */
{																																								/* */
	PORTC &= 0x0f;																																/* Clears the 2nd 4 bits */
	PORTC &= (~(1<<2));																														/* clears the 2nd bit to turn off the 1nd 7-seg */
	PORTC |= (1<<3);																															/* sets the 3rd bit to 1 to turn on the 2nd 7-seg */
	PORTC |= (data<<4);																														/* sends the data to the 2nd 4 bits */
}																																								/* */
																																								/* */
void Sev_Seg_Both_Write(char data1, char data2)																	/* Writes to both 7-Seg */
{																																								/* */
	PORTC &= 0x0f;																																/* Clears the 2nd 4 bits */
	PORTC &= (~(1<<3));																														/* clears the 3rd bit to turn off the 2nd 7-seg */
	PORTC |= (1<<2);																															/* sets the 2nd bit to 1 to turn on the 1st 7-seg */
	PORTC |= (data1<<4);																													/* sends the data to the 2nd 4 bits */
	_delay_ms(1);																																	/* Delay 1ms */
	PORTC &= 0x0f;																																/* Clears the 2nd 4 bits */
	PORTC &= (~(1<<2));																														/* clears the 2nd bit to turn off the 1nd 7-seg */
	PORTC |= (1<<3);																															/* sets the 3rd bit to 1 to turn on the 2nd 7-seg */
	PORTC |= (data2<<4);																													/* sends the data to the 2nd 4 bits */
	_delay_ms(1);																																	/* delay 1ms */
}																																								/* */
/***********************************************************************************/
