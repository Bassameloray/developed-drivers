/******************************************* ADCHeader File ****************************************************************/
#ifndef ADC_DRIVER_H_																									/* */
#define ADC_DRIVER_H_																									/* */
#define F_CPU 8000000UL																									/* Delay Cycle*/
#include <util/delay.h>																									/* Delay Header*/
/**************************************** Magic Numbers' Macro *************************************************************/
#define ADPS0 0																											/* */
#define ADPS1 1																											/* */
#define ADPS2 2																											/* */
#define PERCANT_50 0x0100																								/* */
#define PERCANT_100 0x0200																								/* */
/************************************* End Of Magic Numbers' Macro *********************************************************/
/**************************************** Functions Prototypes *************************************************************/
	void ADC_vInit(void);																								/* Initializing The ADC*/
	short ADC_u16Read(void);																							/* Reading Data*/
	void ADC_vStartConversion(void);																					/* Starting Conversion*/
/******************************************* End Of Texx *******************************************************************/
#endif /********************************************************************************************************************/