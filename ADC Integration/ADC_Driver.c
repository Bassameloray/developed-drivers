/********************************************* Including Header Files **************************************/
#include "DIO.h"																						/* Including All The Header Files.*/
#include "ADC_Driver.h"																					/* Including The ADC Prototypes */
#define F_CPU 8000000UL																					/* Delay Cycle*/
#include <util/delay.h>																					/* Delay Header*/
/*********************************************** Function Delaration ***************************************/
void ADC_vInit()																						/* Initializing The Analog Digital Conversion.*/
{																										/* */
	ADMUX  |= (HIGH<<REFS0); 																			/* Making The Maximum Value The VCC */																							/* Enabling The Bit 7 Which Is Responsible To Make The Register Work*/
	ADCSRA |= (HIGH<<ADPS0);																			/* Enables The Bits 0,1,2 To Make The Division Factor 128*/
	ADCSRA |= (HIGH<<ADPS1);																			/* Enables The Bits 0,1,2 To Make The Division Factor 128*/
	ADCSRA |= (HIGH<<ADPS2);																			/* Enables The Bits 0,1,2 To Make The Division Factor 128*/
	ADCSRA |= (HIGH<<ADEN);																				/* Enabling The ADC By Writing 1 To This Bit (Bit 7)*/
}																										/* */
																										/* */
short ADC_u16Read(void)																					/* Function That Reads Data*/
{																										/* */
	short Read_Var;																						/* Short Variable To Save 16 Bit*/
	ADCSRA |= (HIGH<<ADSC);																				/* Writing 1 To This Bit To Start Conversion*/
	while(((ADCSRA&(HIGH<<ADIF))>>ADIF) == LOW)															/* To Delay The Function Until ADIF Is 1 ( Until A Data Comes)*/
	{																									/* */
		;																								/* */
	}																									/* */
	Read_Var = (ADCL);																					/* Adding The ADCL To This First 8 Bits*/
	Read_Var |= (ADCH<<BIT8);																			/* Adding The ADCH To The Last 8 Bits At The Same Variable With ADCL As The First 8 Bits*/
	return Read_Var;																					/* Returns The Read Value */
}																										/* */
																										/* */
	void ADC_vStartConversion()																			/* A Function The Start The Conversion Operation*/
	{																									/* */
		 short Return_Val=LOW;																			/* Initializing Variable Of 16 Bits*/
		 _delay_ms(5);																					/* Making Small Delay (5 MS)*/
		 Return_Val = ADC_u16Read();																	/* Reading The ADCH And ADCL Then Adding Them To The Short Variable*/
		 PORTC = (char)Return_Val;																		/* Taking Only The First 8 Bits Of The Variable And Adding Them To Port B*/
		 if((Return_Val& PERCANT_50) !=LOW)																/* If The Variable Is Between Zero And 50% */
		 {																								/* */
			 PORTA |= (HIGH<<BIT7);																		/* PORTA Bit7 Will Be High*/
		 }																								/* */
		 else/* if((Return_Val& PERCANT_50) ==LOW)*/													/* Else*/
		 {																								/* */
			 PORTA &= ~(HIGH<<BIT7);																	/* PORTA BIT7 Will Be Low*/
		 }																								/* */
		 if((Return_Val & PERCANT_100)!=LOW)															/* If The Variable Is Between 50% And 100%*/
		 {																								/* */
			 PORTA |= (HIGH<<BIT6);																		/* PORTA BIT6 Will Be HIGH*/
		 }																								/* */
		 else /*if((Return_Val& PERCANT_100)==LOW)*/													/* Else*/																					/* */																					/* */																		/* */
		 {																								/* */
			 PORTA &= ~(HIGH<<BIT6);																	/* PORTA BIT6 Will Be Low*/
		 }																								/* */
	 }																									/* */
/*************************************************** End Of Text ********************************************/