/************************************************ Timer Header File ************************************************/
#ifndef TIMER_DRIVER_H_																							/* */
#define TIMER_DRIVER_H_																							/* */
/********************************************** Functions Prototyping **********************************************/
void Timer_vInit(void);																							/* Function To Initialize The Timer At CTC Mode*/
void Timer_vInit_FPWM(void);																				/* Function To Initialize The Timer At Fast PWM Mode*/
void Timer_vSetDuty(double Percant);																/* */
/************************************************** End Of Text ****************************************************/
#endif /************************************************************************************************************/
