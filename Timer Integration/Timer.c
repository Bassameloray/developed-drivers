/******************************************* Including Requierd Header Files *******************************************/
#include "DIO.h"																											/* Including All Header Files*/
#include "LCD_Driver.h"																								/* LCD Functions Prototypes */
#include "KeyPadDriver.h"																							/* */
#include "Timer_Driver.h"																							/* */
#include "ADC_Driver.h"																								/* */
/*************************************************** Main APP Body *****************************************************/
volatile double x = 0;																								/* */
int main(void)																												/* */
{																																			/* */
	Timer_vInit_FPWM();																									/* */
	DIO_VSetPinDir('B',3,'h');																					/* */
	sei();																															/* */
    while(1)																													/* */
    {																																	/* */
		_delay_ms(1000);																									/* */
    }																																	/* */
}																																			/* */
ISR(TIMER0_OVF_vect)																									/* */
{																																			/* */
	DDRD = 0xff;																												/* */
	PORTD ^= 0XFF;																											/* */
	x=x+10;																															/* */
	Timer_vSetDuty(x);																									/* */
	if (x>=100)																													/* */
	{x=0;																																/* */
	}																																		/* */
																																			/* */
}																																			/* */
/*************************************************** End Of Text *******************************************************/
/************************************************** Comparing APP ******************************************************/
//volatile char Count=0;																						/* Volatile To Get Rid Of The Optimization*/
//int main(void)																										/* */
//{																																	/* */
//	Timer_vInit_CTC();																							/* Initializing The Timer*/
//	DIO_VSetPinDir('D',0,'h');																			/* Sets The PORTD Bit 0 To Output*/
//	sei();																													/* The Interrupt Initialization*/
//	while(1)																												/* */
//	{																																/* */
//		if(Count==1)																									/* After 100 Cycle*/
//		{																															/* */
//			TOGGLE_BIT(PORTD,0);																				/* It Will Toggles The LED Connected On PORTD Bit 0*/
//			Count=0;																										/* Resets The Counter*/
//		}																															/* */
//	}																																/* */
//}																																	/* */
//ISR(TIMER0_COMP_vect)																							/* The Interrupt Function*/
//{																																	/* */
//	Count++;																												/* Increments The Timer By 1 After Every Cycle Of The Comparing Timer*/
//	TCNT0 = 0;																											/* Resets The Flag Of The Timer*/
//}																																	/* */
/********************************************* End Of Text *************************************************************/
