/*********************************************** Timer Driver C File *******************************************************/
#include "DIO.h"																											/* Including All Header Files*/
#include "LCD_Driver.h"																								/* LCD Functions Prototypes */
#include "KeyPadDriver.h"																							/* */
#include "Timer_Driver.h"																							/* */
#include "ADC_Driver.h"																								/* */
/********************************************** Functions Declaration ******************************************************/
																																			/* */
void Timer_vInit_CTC(void)																						/* Function To Initialize The Timer At CTC Mode*/
{																																			/* */
	TCCR0 |= (HIGH<<WGM01);																							/* Setting The Counter Mode To CTC*/
	TCCR0 |= (HIGH<<CS00);																							/* Setting The Clock Cycle To 1024*/
	TCCR0 |= (HIGH<<CS02);																							/* Setting The Clock Cycle To 1024*/
	OCR0 = 78;																													/* Setting The Comparing Value To 78*/
	TIMSK |= (HIGH<<OCIE0);																							/* Enabling The Timer Output Comparing Mode*/																														/* */
}																																			/* */
																																			/* */
void Timer_vInit_FPWM(void)																						/* Function To Initialize The Timer At Fast PWM Mode*/
{																																			/* */
	TCCR0 |= (HIGH<<WGM00);																							/* Setting The Counter Mode To FPWM*/
	TCCR0 |= (HIGH<<WGM01);																							/* */
	TCCR0 |= (HIGH<<CS00);																							/* Setting The Clock Cycle To 1024*/
	TCCR0 |= (HIGH<<CS02);																							/* */
	TCCR0 |= (1<<COM01);																								/* Setting The Clock Cycle To 1024*/
	TIMSK |= (HIGH<<TOIE0);																							/* Enabling The Timer Output Timer/Counter0 Overflow Interrupt*/																														/* */
																																			/* */
	OCR0 = 127;																													/* */
																																			/* */
}																																			/* */
																																			/* */
																																			/* */
void Timer_vSetDuty(double Percant)																		/* To Edit The Speed*/
{																																			/* */
	OCR0 = ((Percant*255)/100);																					/* Setting The Comparing Value To The Desired Percant*/
}																																			/* */
/************************************************* End Of Text *************************************************************/
