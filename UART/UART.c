/********************************************* Including The Header Files **************************************************/
#include "UART.h"																											/* */
#define F_CPU 8000000ul																								/* */
#include <avr/io.h>																										/* */
/*********************************************** Functions Declaration *****************************************************/
void UART_vInit(long baud)																						/* */
{																																			/* */
	short UBRR=0;																												/* */
	/*1 - Choose baud rate that will be used by sender and receiver by writing to UBRRL/UBRRH*/							/* */
	UBRR = (((F_CPU)/(16*baud))-1);																			/* Calculating The Equation*/
	UBRRH |= (char)(((UBRR&0xff00)>>8));																/* 2nd Byte*/
	UBRRL |= (char)((UBRR&0x00ff));																			/* First Byte*/
	/*2 - Enable UART Sender & Receiver*/																/* */
	UCSRB |= (1<<RXEN);																									/* Enable The Receiver Mode*/
	UCSRB |= (1<<TXEN);																									/* Enable The Transmition Mode*/
	/*3 - Choose as	asynchronous or synchronous from UCSRC.*/						/* */
	//UCSRC |= (1<<URSEL);																							/*Reading UCSRC And Enable The Writing On It*/
	//UCSRC |= (1<<USBS);																								/* Number Of Stop Bits*/
	//UCSRC |= (0<<UMSEL);																							/* Setting The Operation To ASynchronous */
	/*4 - Choose number of data bits to be sent from UCSRC We will work with 8 bits.*/									/* */
	UCSRC |= (1<<UCSZ0);																								/* Setting The Bit Settings To 8-Bits Mode*/
	UCSRC |= (1<<UCSZ1);																								/* Setting The Bit Settings To 8-Bits Mode*/
	//UCSRC |= (0<<UCSZ2);																							/* Setting The Bit Settings To 8-Bits Mode*/
}																																			/* */
																																			/* */
void UART_vSendData(char data)																				/* */
{																																			/* */
	char Temp=0;																												/* */
	/*Wait for Transmit buffer to be empty*/														/* */
	while((UCSRA&(1<<UDRE))==0)																					/* Reading The Value Of TXC*/
	{																																		/* */
																																			/* While TXC Is Not 1 Do Nothing And Loop Again*/
	}																																		/* */
	/*Put data to transmit buffer*/																			/* */
	UDR = data;																													/* Writing Data To UDR*/
}																																			/* */
char UART_u8ReceiveData()																							/* */
{																																			/* */
	/*Wait for receive buffer to be filled with data*/									/* */
		while((UCSRA&(1<<RXC))==0)																				/* Reading The Value Of TXC*/
		{																																	/* */
																																			/* While RXC Is 0 Do Nothing And Loop Again*/
		}																																	/* */
	/*Receive data from Data Buffer*/																		/* */
	return UDR;																													/* Return Data*/
}																																			/* */
/**************************************************** End Of Text **********************************************************/
