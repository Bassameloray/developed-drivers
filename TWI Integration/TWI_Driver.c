/*
 * TWI_Driver.c
 *
 * Created: 9/13/2017 11:07:08 PM
 *  Author: Cennan
 */ 


#include <avr/io.h>
#include "TWI.h"
#define F_CPU 8000000ul
#include <util/delay.h>

int main(void)
{
	I2C_master_Init();
    while(1)
    {
		_delay_ms(1000);
        I2C_start(0x5);
        I2C_write('B');
        I2C_stop();
		_delay_ms(7000);
    }
}
