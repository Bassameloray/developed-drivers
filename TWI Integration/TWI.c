#include "TWI.h"
#define F_CPU 8000000UL
#define F_SCL 400000UL // SCL frequency
#define Prescaler 1
#define TWBR_val ((((F_CPU / F_SCL) / Prescaler) - 16 ) / 2)
/* SLA+W has been received;
 * ACK has been returned */
#define TW_SR_SLA_ACK 0x60


void I2C_master_Init()
{
	/*Choose value of TWBR register from the above equation*/
	TWBR = (char)TWBR_val;
}

void I2C_start(char address)
{
	/*Clear TWCR First*/
	TWCR = 0;
	/*Set TWINT so that it will be cleared by hardware*/
	TWCR = (1<<TWINT);
	/*Enable TWI from TWE*/
	TWCR |= (1<<TWEN);
	/*TWCR = 1;*/	
	/*Write the TWSTA bit to one to send start bit*/
	TWCR |= (1<<TWSTA);
	/*Wait until transmission is finished*/
	while((TWCR&(1<<TWINT)) == 0)
	{
		
	}
	/*Send the 7 bit address on TWDR*/
	TWDR = address<<1;
	/*Start Transmission*/
	TWCR |= (1<<TWINT); 	
	/*Wait until transmission is finished*/
	while((TWCR&(1<<TWINT)) == 0)
	{
		
	}
	
}

void I2C_write(char data)
{
	/*Load data you need to send in TWDR*/
	TWDR = data;
	/*Start Transmission*/
	TWCR |= (1<<TWINT);
	/*Wait until transmission is finished*/
	while((TWCR&(1<<TWINT)) == 0)
	{
		
	}	
	
}


void I2C_stop(void)
{
	/*Set TWINT so that it will be cleared by hardware*/
	TWCR = (1<<TWINT);
	/*Enable TWI from TWE*/
	TWCR |= (1<<TWEN);
	/* transmit STOP condition */
	TWCR |= (1<<TWSTO);
}

char I2C_read(void)
{
	/*Set TWINT so that it will be cleared by hardware*/
	TWCR |= (1<<TWINT);
	/*Enabe TWI from TWEN bit*/
	TWCR |= (1<<TWEN);
	/*Enable Acknowledge bit enable*/
	TWCR |= (1<<TWEA);
	/*Wait until transmission is finished*/
	while((TWCR&(1<<TWINT)) == 0)
	{
		
	}	
	/*Read received data from resgister TWDR*/
	return TWDR;
}

void I2C_Slave_Init(char my_Address)
{
	/*Tell the slave that this is his address*/
	TWAR = my_Address<<1;
	/*Set TWINT so that it will be cleared by hardware*/
	TWCR |= (1<<TWINT);
	/*Enabe TWI from TWEN bit*/
	TWCR |= (1<<TWEN);
	/*Enable Acknowledge bit enable*/
	TWCR |= (1<<TWEA);
}

char I2C_Slave_avialable()
{
	/*Set TWINT so that it will be cleared by hardware*/
	TWCR |= (1<<TWINT) | (1<<TWEN);
	/*Enabe TWI from TWEN bit*/
	
	/*Wait until transmission is finished*/
	while(!(TWCR&(1<<TWINT)))
	{
		
	}
	/*Return true if TWSR is equal to 0x60 TW_SR_SLA_ACK*/
	return ((TWSR & 0xF8) == TW_SR_SLA_ACK);
}
