/*
 * TWI.h
 *
 * Created: 9/13/2017 11:20:48 PM
 *  Author: Cennan
 */ 


#ifndef TWI_H_
#define TWI_H_
#include <avr/io.h>

/*Master*/
void I2C_master_Init();
void I2C_start(char address);
void I2C_write(char data);
void I2C_stop(void);
/*Slave*/
char I2C_read(void);
void I2C_Slave_Init(char my_Address);
char I2C_Slave_avialable();


#endif /* TWI_H_ */