#include "DIO.h"
/****************************************************** Ultrasonic Driver ****************************************************/
void US_Initialization(void)
{
        DIO_VSetPinDir( 'D' , PORT0 , HIGH );                                   /* For The Trigger */
        DIO_VSetPinDir( 'D' , PORT2 , HIGH );                                   /* For The Echo Interrupt 0 PIN */
        DIO_VSetPort_IO( 'D' , PORT0 ,  OUTPUT );                               /* This PIN Will send the trigger wave */
        DIO_VSetPort_IO( 'D' , PORT2 , INPUT );                                 /* This Pin Will Recieve The Interrupt and The Echo */

}
