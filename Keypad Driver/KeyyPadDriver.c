/************************************************ Including Header Files ***********************************************************/
#include <avr/io.h>																												/* Main Input/Output Header*/
#define F_CPU 8000000UL																											/* Delay Cycle*/
#include <util/delay.h>																											/* Delay Header*/
#include <avr/interrupt.h>																										/* The Interrupt Header File*/
#include "DIO.h"																												/* ATMega-32 Drivers*/
#include "LCD_Driver.h"																											/* LCD Functions*/
#include "KeyPadDriver.h"																										/* KeyPad Functions*/
/************************************************ Function Declaration *************************************************************/
char MyArray[4][4]={{'7','8','9','-'},{'4','5','6','*'},{'1','2','3','-'},{'A','0','=','+'}};									/* Defining The Buttons*/												
																																/* */
void KeyPad_vInit()																												/* Initializing The Keypad*/
{																																/* */
																																/* */
	DIO_VSetPinDir('D',0,'h');																									/* Setting The First 4 Bits Of PORTD To Output*/
	DIO_VSetPinDir('D',1,'h');																									/* Setting The First 4 Bits Of PORTD To Output*/
	DIO_VSetPinDir('D',2,'h');																									/* Setting The First 4 Bits Of PORTD To Output*/
	DIO_VSetPinDir('D',3,'h');																									/* Setting The First 4 Bits Of PORTD To Output*/
	DIO_VSetPinDir('D',4,'l');																									/* Setting The 2nd 4 Bits Of PORTD To Input*/
	DIO_VSetPinDir('D',5,'l');																									/* Setting The 2nd 4 Bits Of PORTD To Input*/
	DIO_VSetPinDir('D',6,'l');																									/* Setting The 2nd 4 Bits Of PORTD To Input*/
	DIO_VSetPinDir('D',7,'l');																									/* Setting The 2nd 4 Bits Of PORTD To Input*/
	OPEN_PULL_UP_RESISTANCE;																									/* Using Poll Up Resistance To Get Rid Of The Floating Points*/
	DIO_VSetPort_IO('D',4,'h');																									/* Writing 1 To The 2nd 4 Bits*/
	DIO_VSetPort_IO('D',5,'h');																									/* Writing 1 To The 2nd 4 Bits*/
	DIO_VSetPort_IO('D',6,'h');																									/* Writing 1 To The 2nd 4 Bits*/
	DIO_VSetPort_IO('D',7,'h');																									/* Writing 1 To The 2nd 4 Bits*/
}																																/* */
																																/* */
char Keypad_u8press(void)																										/* The Function Loops And Takes The Input From The Keypad*/
{																																/* */										
	short  row = Zero_ROW;																										/* */																
	for(row == Zero_ROW;row<FIFTH_ROW;row++)																					/* Starting At First Row And Ending At The Third Row*/
	{																															/* */
	DIO_VSetAllPort_IO('D','h');																								/* Resetting The Value Of All Ports To Ones*/
	DIO_VSetPort_IO('D',row,'l');																								/* Setting The Port Of The Same Row Number At PORTD To Zero */	
																																/* */
	if( ((PIND & (HIGH<<KEYPAD_FIRST_COLUMN))>>KEYPAD_FIRST_COLUMN) == SHORT_CIRCUIT)											/* If The First Column Is Shortened*/
	{																															/* */
		return MyArray[row][COLUMN1];																							/* It Will Return The First Column With The Row Number*/
	}																															/* */
	else if( ((PIND & (HIGH<<KEYPAD_SECOND_COLUMN))>>KEYPAD_SECOND_COLUMN) == SHORT_CIRCUIT)									/* If The Second Column Is Shortened*/
	{																															/* */
		return MyArray[row][COLUMN2];																							/* It Will Return The Second Column With The Row Number*/
	}																															/* */
	else if( ((PIND & (HIGH<<KEYPAD_THIRD_COLUMN))>>KEYPAD_THIRD_COLUMN) == SHORT_CIRCUIT)										/* If The Third Column Is Shortened*/
	{																															/* */
		return MyArray[row][COLUMN3];																							/* It Will Return The Third Column With The Row Number*/
	}																															/* */
	else if( ((PIND & (HIGH<<KEYPAD_FORTH_COLUMN))>>KEYPAD_FORTH_COLUMN) == SHORT_CIRCUIT)										/* If The Forth Column Is Shortened*/
	{																															/* */
		return MyArray[row][COLUMN4];																							/* It Will Return The Forth Column With The Row Number*/
	}																															/* */
																																/* */
	}																															/* */
	return NO_INPUT;																											/* Else It Will Return Error Value*/
}																																/* */
/**************************************************** End Of Text ******************************************************************/




