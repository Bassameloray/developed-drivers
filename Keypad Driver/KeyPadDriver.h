/**************************************************** Protecting The File ******************************************************/
#ifndef KEYPADDRIVER_H_																										/* Protecting The File*/
#define KEYPADDRIVER_H_																										/* */
/************************************************** Including Header Files *****************************************************/
#include <avr/io.h>																											/* Main Input/Output Header*/
#define F_CPU 8000000UL																										/* Delay Cycle*/
#include <util/delay.h>																										/* Delay Header*/
#include <avr/interrupt.h>																									/* The Interrupt Header File*/
#include "DIO.h"																											/* Including All Header Files*/
/*************************************************** Magic Numbers' Macro ******************************************************/
#define OPEN_PULL_UP_RESISTANCE SFIOR &= (~(HIGH<<PUD))																		/* Using Poll Up Resistance To Get Rid Of The Floating Points*/
#define NO_INPUT 150																										/* */
#define ASCII_ZERO '48'																										/* */
#define KEYPAD_FIRST_COLUMN 4																								/* */
#define KEYPAD_SECOND_COLUMN 5																								/* */
#define KEYPAD_THIRD_COLUMN 6																								/* */
#define KEYPAD_FORTH_COLUMN 7																								/* */
#define SHORT_CIRCUIT 0																										/* */
/*********************************************** End Of Magic Numbers' Macro ***************************************************/
/************************************************** Functions Prototyping ******************************************************/
void KeyPad_vInit();																										/* Initializing The Keypad*/
char Keypad_u8press();																										/* Taking Input From The Keypad*/
/****************************************************** End Of Text ************************************************************/
#endif /************************************************************************************************************************/