
#include "EEPROM_App.h"
#include <avr/io.h>

void EEPROM_vWrite(char data, short address)
{
	/*wait until EEWE bit is cleared that means that the previous write is finished */
	while(EECR & (1<<EEWE))
	{
		
	}
	/*Now we are ready to write data*/
	
	/*Choose the address we will write to*/
	EEAR = address;
	/*Put data that needs to be written in data register*/
	EEDR = data;
	/*Set master write enable bit*/
	EECR |= (1<<EEMWE);
	/*Set eeprom write enable*/
	EECR |= (1<<EEWE);
}
char EEPROM_u8Read(short address)
{
	/*wait until EEWE bit is cleared that means that the previous write is finished */
	while(EECR & (1<<EEWE))
	{
		
	}
	/*Choose the address we will need to read from*/
	EEAR = address;
	/*Enable read from EEPROM*/
	EECR |= (1<<EERE);
	/* Return data from data register */
	return EEDR;
}
